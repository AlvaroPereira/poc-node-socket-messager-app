import * as http from "http";
import WebSocket, { WebSocketServer } from "ws";

const server = http.createServer();

const wss = new WebSocketServer({server});

wss.addListener("connection", (connection) => {
    console.log("New connection was created");
    
    connection.addEventListener("message", () => {
        wss.clients.forEach((client) => {
            if(client.readyState !== WebSocket.OPEN) return;
        })
    })
    
    connection.addEventListener("close", () => {
        console.log("An connection was closed");
    })
});

const port = 3000;

server.listen(port, () => {
    console.log(`Server are runnig on port: ${port}`);
})