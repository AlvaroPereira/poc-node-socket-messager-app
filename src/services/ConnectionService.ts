export default class ConnectionService {
    private static connections: Set<string> = new Set();

    static getConnections(): Promise<Set<string>> {
        return Promise.resolve(this.connections);
    }

    static getConnection(connectionId: string): Promise<string | null> {
        const connection = this.connections.has(connectionId);
        return Promise.resolve(connection ? connectionId : null);
    }

    static addConnection(connectionId: string): void {
        if(connectionId.length == 0) return;
        this.connections.add(connectionId);
    }

    static removeConnection(connectionId: string): void {
        this.connections.delete(connectionId);
    }
    
    static clearConnections(): void {
        this.connections.clear();
    }
}