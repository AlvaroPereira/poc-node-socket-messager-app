import ConnectionService from '../src/services/ConnectionService';

describe('ConnectionService', () => {
    describe('getConnection', () => {
        beforeEach(async () => {
            await ConnectionService.addConnection('f8446571-ed47-4448-a949-71c06803a8a9');
        });
        
        afterAll(async () => {
            ConnectionService.clearConnections();
        })

        it('Connection is not found', async () => {
            const connection = await ConnectionService.getConnection('01a517f5-0797-4802-ad74-7080530b586c');
            expect(connection).toEqual(null);
        });

        it('Found a valid connection', async () => {
            const connectionId = 'f8446571-ed47-4448-a949-71c06803a8a9';
            const connection = await ConnectionService.getConnection(connectionId);
            expect(connection).toEqual(connectionId);
        });
    });

    describe('addConnection', () => {
        it('Should add a connection', async () => {
            const connectionId = '577f4545-5540-423f-ba08-28292131fc52';
            await ConnectionService.addConnection(connectionId);
            const connection = await ConnectionService.getConnection(connectionId);
            expect(connection).toEqual(connectionId);
        });

        it('Should not add a connection that already exists', async () => {
            const connectionId = 'a1b2c3d4-e5f6-7890-a1b2-c3d4e5f67890';
            await ConnectionService.addConnection(connectionId);
            const initialCount = (await ConnectionService.getConnections()).size;
            await ConnectionService.addConnection(connectionId);
            const finalCount = (await ConnectionService.getConnections()).size;
            expect(finalCount).toEqual(initialCount);
        });

        it('Should not add a connection with an empty string', async () => {
            const connectionId = '';
            await ConnectionService.addConnection(connectionId);
            const connection = await ConnectionService.getConnection(connectionId);
            expect(connection).toEqual(null);
        });
    });

    describe('removeConnection', () => {
        beforeEach(async () => {
            await ConnectionService.addConnection('ebef32c9-9d2c-4183-9361-643f08f6c20a');
        });
        
        afterAll(async () => {
            ConnectionService.clearConnections();
        })

        it('Should remove a connection', async () => {
            const connectionId = 'ebef32c9-9d2c-4183-9361-643f08f6c20a';
            await ConnectionService.removeConnection(connectionId);
            const connection = await ConnectionService.getConnection(connectionId);
            expect(connection).toEqual(null);
        });

        it('Should not remove a connection that does not exist', async () => {
            const connectionId = 'a1b2c3d4-e5f6-7890-a1b2-c3d4e5f67890s';
            const initialCount = (await ConnectionService.getConnections()).size;
            await ConnectionService.removeConnection(connectionId);
            const finalCount = (await ConnectionService.getConnections()).size;
            expect(finalCount).toEqual(initialCount);
        });

        it('Should not remove a connection with an empty string', async () => {
            const connectionId = '';
            const initialCount = (await ConnectionService.getConnections()).size;
            await ConnectionService.removeConnection(connectionId);
            const finalCount = (await ConnectionService.getConnections()).size;
            expect(finalCount).toEqual(initialCount);
        });
    });
});
